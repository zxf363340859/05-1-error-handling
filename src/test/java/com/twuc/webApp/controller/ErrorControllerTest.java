package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
public class ErrorControllerTest {

    @Autowired
    TestRestTemplate testRestTemplate;

    @Test
    void should_throw_exception() throws Exception {
        ResponseEntity<String> forEntity = testRestTemplate.getForEntity("/api/exception/runtime-exception", String.class);
        String body = forEntity.getBody();
        assertThat(body).isEqualTo("handle exceptipon");
        assertThat(forEntity.getStatusCode().is4xxClientError());
    }

    @Test
    void test_handle_null_pointer_exception() throws Exception {
        ResponseEntity<String> forEntity = testRestTemplate.getForEntity("/api/exception/null-pointer-exception", String.class);
        String body = forEntity.getBody();
        assertThat(body).isEqualTo("Something wrong with the argument.");
        assertThat(forEntity.getStatusCode()).isEqualTo(HttpStatus.I_AM_A_TEAPOT);
    }

    @Test
    void test_handle_arithmetic_exception() throws Exception {
        ResponseEntity<String> forEntity = testRestTemplate.getForEntity("/api/exception/arithmatic-exception", String.class);
        String body = forEntity.getBody();
        assertThat(body).isEqualTo("Something wrong with the argument.");
        assertThat(forEntity.getStatusCode()).isEqualTo(HttpStatus.I_AM_A_TEAPOT);
    }

    @Test
    void test_controller_advicer_handle_sister() throws Exception {
        ResponseEntity<String> forEntity = testRestTemplate.getForEntity("/api/sister-errors/illegal-argument", String.class);
        String body = forEntity.getBody();
        assertThat(body).isEqualTo("{\"message\":\"Something wrong with brother or sister.\"}");
        assertThat(forEntity.getStatusCode()).isEqualTo(HttpStatus.I_AM_A_TEAPOT);
    }

    @Test
    void test_controller_advicer_handle_bother() throws Exception {
        ResponseEntity<String> forEntity = testRestTemplate.getForEntity("/api/brother-errors/illegal-argument", String.class);
        String body = forEntity.getBody();
        assertThat(body).isEqualTo("{\"message\":\"Something wrong with brother or sister.\"}");
        assertThat(forEntity.getStatusCode()).isEqualTo(HttpStatus.I_AM_A_TEAPOT);
    }
}
