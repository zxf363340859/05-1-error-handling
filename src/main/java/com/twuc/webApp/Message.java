package com.twuc.webApp;

public class Message {

    private String message;

    public String getMessage() {
        return message;
    }

    public Message() {
    }

    public Message(String message) {
        this.message = message;
    }

}
