package com.twuc.webApp.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControllerLevelController {

    @GetMapping("/api/exception/null-pointer-exception")
    @ResponseBody
    public ResponseEntity getNullPointerException() {
        throw new NullPointerException("this is a null pointer exception");
    }

    @GetMapping("/api/exception/arithmatic-exception")
    @ResponseBody
    public ResponseEntity getArithmaticException() {
        throw new ArithmeticException("this is a arithmeticr exception");
    }

    @ExceptionHandler({NullPointerException.class, ArithmeticException.class})
    public ResponseEntity<String> handleEx() {
        return ResponseEntity.status(418).body("Something wrong with the argument.");
    }


}
