package com.twuc.webApp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.AccessControlException;

@RestController
public class ErrorController {

    @GetMapping("/api/exception/runtime-exception")
    @ResponseBody
    public ResponseEntity getError() {
        throw new RuntimeException("runtime exception===========_)++_++_++__+");
    }

    @GetMapping("/api/exception/access-control-exception")
    @ResponseBody
    public ResponseEntity getAccessControlException() {
        throw new AccessControlException("_)(*&^%$$^*((");
    }

    @ExceptionHandler
    public ResponseEntity<String> hanld(RuntimeException ex) {
        return ResponseEntity.status(404).body("handle exceptipon");
    }

    @ExceptionHandler(AccessControlException.class)
    public ResponseEntity<String> hanld() {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("handle AccessControlException exceptipon");
    }

}
