package com.twuc.webApp.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BotherController {

    @GetMapping("/api/brother-errors/illegal-argument")
    @ResponseBody
    public RequestBody getNullPointerException() {
        throw new IllegalArgumentException("this is brother controller");
    }

}
