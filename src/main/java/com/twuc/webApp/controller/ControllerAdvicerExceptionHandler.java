package com.twuc.webApp.controller;

import com.twuc.webApp.Message;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ControllerAdvicerExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Object> handleEx(RuntimeException ex, WebRequest request) {


        return handleExceptionInternal(ex, new Message("Something wrong with brother or sister."), new HttpHeaders(), HttpStatus.I_AM_A_TEAPOT, request);
    }

}
