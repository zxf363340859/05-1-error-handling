package com.twuc.webApp.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SisterController {

    @GetMapping("/api/sister-errors/illegal-argument")
    @ResponseBody
    public RequestBody getNullPointerException() {
        throw new IllegalArgumentException("this is sister controller");
    }

}
